<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/thank-you', 'HomeController@thankyou');

Auth::routes();
Route::get('/log-in', 'HomeController@log_in');
Route::get('/', 'HomeController@log_in');
Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' =>'admin_auth','prefix' => 'admin'], function(){
    Route::get('/dashboard', 'AdminController@index');
    Route::get('/pass', 'AdminController@pass');
    Route::get('/reports', 'AdminController@reports');
    Route::post('/reports/filter', 'AdminController@report_range')->name('report_range');

    Route::get('/users', 'UserController@view_users');
    Route::post('/user/add', 'UserController@add_user')->name('add_user');
    Route::get('/user/delete/{user_id}', 'UserController@delete_user')->name('delete_user');
    Route::get('/filter_report', 'AdminController@filter_report')->name('filter_report');
    Route::get('/list', 'PassController@list');

    Route::post('/pass/add', 'PassController@add_pass')->name('add_pass');
    Route::post('/pass/traveller/add', 'PassController@add_traveller_pass')->name('add_traveller_pass');
   
    Route::get('/traveller/{traveller_id}', 'TravellerController@view_traveller')->name('view_traveller');
    Route::get('/traveller/edit/{traveller_id}', 'TravellerController@edit_traveller')->name('edit_traveller');
    Route::post('/traveller/update', 'TravellerController@update_traveller')->name('update_traveller');
   
    Route::get('/pass_search', 'PassController@pass_search')->name('pass_search');


});




Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
