<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Response;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\Brgyuser;

class UserController extends Controller
{
    //
    public function add_user(Request $request)
    {
        $rules = array(
            'fullname' => 'required|string|max:255',
            'username' => 'required|string|max:255|unique:users',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required'
        );
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            return back()->withErrors($validator->getMessageBag());

        } else {
            $data_user = new User();
            $data_user->name = strtoupper($request['fullname']);
            $data_user->username = $request['username'];
            $data_user->email = $request['email'];
            $data_user->password = Hash::make($request['password']);
            $data_user->usertype = 'brgy';
            $data_user->status = 'active';
            $data_user->save();
            //return $data_user;

            $data_bry = new Brgyuser();
            $data_bry->user_id = $data_user->id;
            $data_bry->brgy = $request['brgy'];
            $data_bry->save();
            return redirect()->back()->with('success','User successfully added!');
        }
    }
    
    public function delete_user($user_id)
    {
        User::find($user_id)->delete();
        $updateBrgyuser = Brgyuser::where('user_id', '=', $user_id)
                    ->delete();
        return redirect()->back()->with('warning','User successfully deleted!');
    }

    public function view_users(Request $req)
    {
        $data_user = Brgyuser::with('user')->get();
    
        return view('admin.users', compact('data_user'));
    }
}
