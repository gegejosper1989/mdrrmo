<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Traveller;
use App\Pass;

class TravellerController extends Controller
{
    //

    public function view_traveller($traveller_id){
        $data_traveller = Traveller::with('pass')->where('id', '=', $traveller_id)->first();
        //dd($data_traveller);
        return view('admin.traveller', compact('data_traveller'));
    }

    public function edit_traveller($pass_id){
        $data_pass = Pass::with('traveller')->where('id', '=', $pass_id)->first();
        return view('admin.traveller-edit', compact('data_pass'));
    }

    public function update_traveller(Request $req){
        $updatePass = Pass::where('id', '=', $req->pass_id)
                    ->update(['destination' => $req->destination, 'purpose' => $req->purpose, 
                    'valid_date_from' => $req->from, 'valid_date_to' => $req->to,
                    'pass_type' => $req->pass_type]);
        $updateTraveller = Traveller::where('id', '=', $req->travel_id)
                    ->update(['fname' => $req->fname, 'lname' => $req->lname, 
                    'mname' => $req->mname]);
        return redirect()->back()->with('success','Pass successfully updated!');
    }
}
