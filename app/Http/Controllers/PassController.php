<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pass;
use App\Traveller;
use Illuminate\Support\Facades\Hash;
use Validator;
use DateTime;
use Response;
use DB; 
use Illuminate\Support\Facades\Input;

class PassController extends Controller
{
    //
    public function add_pass(Request $req){
        
        $rules = array(
            'fname' => 'required|string|max:255',
            'lname' => 'required|string|max:255',
        );
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            return back()->withErrors($validator->getMessageBag());
        }
        else {
            $data_traveller = new Traveller();
            $data_traveller->fname = $req['fname'];
            $data_traveller->lname = $req['lname'];
            $data_traveller->mname = $req['mname'];
            $data_traveller->save();

            $data_pass = new Pass();
            $data_pass->traveller_id = $data_traveller->id;
            $data_pass->destination = $req->destination;
            $data_pass->purpose = $req->purpose;
            $data_pass->valid_date_from = $req->from;
            $data_pass->valid_date_to= $req->to;
            $data_pass->pass_type= $req->pass_type;
            $data_pass->save();
        }
        return redirect()->back()->with('success','Pass successfully added!');
    }
    public function add_traveller_pass(Request $req){

            $data_pass = new Pass();
            $data_pass->traveller_id = $req->traveller_id;
            $data_pass->destination = $req->destination;
            $data_pass->purpose = $req->purpose;
            $data_pass->valid_date_from = $req->from;
            $data_pass->valid_date_to= $req->to;
            $data_pass->pass_type= $req->pass_type;
            $data_pass->save();

        return redirect()->back()->with('success','Pass successfully added!');
    }
    public function pass_search(Request $request){
        if($request->ajax())
        {
            $search = $request->search;
            $output="";
            $data_traveller = DB::table('travellers')
                ->where(function($query) use ($search){
                    $query->where('travellers.fname', 'LIKE', '%'.$search.'%');
                    $query->orWhere('travellers.lname', 'LIKE', '%'.$search.'%');
                    $query->orWhere('travellers.mname', 'LIKE', '%'.$search.'%');
                })
                ->latest()
                ->get();
            if($data_traveller)
            {
                foreach ($data_traveller as $Traveller) {
                    $output.='<tr>
                        <td><a href="/admin/traveller/'.$Traveller->id.'">'.strtoupper($Traveller->lname).', '.strtoupper($Traveller->fname).' '.strtoupper($Traveller->mname).'</td>
                        <td><a href="/admin/traveller/'.$Traveller->id.'" class=" btn btn-info btn-small"><i class="btn-icon-only fas fa-search"> </i></a></td>';
                    $output .='</tr>';
                }
                return Response($output);
            }
        }
    }
}
