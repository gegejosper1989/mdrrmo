<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Traveller;
use App\Pass;
use Carbon\Carbon;
use DateTime;
class AdminController extends Controller
{
    //
    public function index(){
        $data_pass = Pass::count();
        $today = date("Y-m-d"); 
        $startDate = Carbon::parse($today.' 00:00:00');
        $endDate = Carbon::parse($today .' 23:59:59'); 
        $data_pass_daily = Pass::whereBetween('created_at', [$startDate, $endDate])->count();
        return view('admin.dashboard', compact('data_pass', 'data_pass_daily'));
    }

    public function pass(){
        $data_traveller = Traveller::take(5)->latest()->get();
        $data_pass = Pass::with('traveller')->latest()->paginate(50);
        return view('admin.pass', compact('data_traveller', 'data_pass'));
    }

    public function reports(){
        $data_pass = Pass::with('traveller')->latest()->paginate(100);
        $data_pass_count = Pass::count();
        return view('admin.reports', compact('data_pass', 'data_pass_count'));
    }

    public function report_range(Request $req){
        $org_from_date = $req->from;  
        $new_from_date = date("Y-m-d", strtotime($org_from_date)); 
        
        $org_to_date = $req->to;  
        $new_to_date = date("Y-m-d", strtotime($org_to_date)); 
        
        $startDate = Carbon::parse($new_from_date.' 00:00:00');
        $endDate = Carbon::parse($new_to_date .' 23:59:59'); 
        $data_pass = Pass::whereBetween('created_at', [$startDate, $endDate])->latest()->paginate(100);
        $data_pass_count = Pass::whereBetween('created_at', [$startDate, $endDate])->count();
        return view('admin.reports', compact('data_pass', 'data_pass_count'));
    }

    public function report_type($type){
        $type = strtoupper($type);
        
        $data_patient = Traveller::where('patient_type', '=', $type)->latest()->get();
        return view('admin.reports', compact('data_patient'));
    }
    public function filter_report(Request $req){
        $type = $req->patient_type;
        $brgy = strtoupper($req->brgy);
        if($type=="ALL" && $brgy != "ALL"){
            $data_patient = Traveller::latest()->get();
        }
        elseif($type !="ALL" && $brgy == "ALL"){
            $data_patient = Traveller::where('patient_type', '=', $type)->latest()->get();
        }
        elseif($type == "ALL" && $brgy == "ALL"){
            $data_patient = Traveller::latest()->get();
        }
        else{
            $data_patient = Traveller::where('patient_type', '=', $type)
                ->where('brgy', '=', $brgy)
                ->latest()->get();
        }    

        
        return view('admin.reports', compact('data_patient'));
    }

    
}
