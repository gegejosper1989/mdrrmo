<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pass extends Model
{
    //
    public function traveller()
    {
        return $this->belongsTo('App\Traveller','traveller_id','id');
    }
}
