<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Traveller extends Model
{
    //
    public function pass()
    {
        return $this->hasMany('App\Pass','traveller_id')->latest();
    }
}
