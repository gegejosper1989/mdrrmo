@extends('layout.admin')

@section('content')
<script src="{{ asset('js/jquery.min.js') }}"></script>
<div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Pass</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Pass</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-md-3">
                <div class="input-group">
                    <input id="search" type="text" type="search" name="search" class="form-control" placeholder="Search">
                    {{ csrf_field() }}
                </div>
                <table class="table table-striped">
                  <thead>
                    <tr>
                      <th>Name</th> 
                    </tr>
                  </thead>
                  <tbody class="passresult">
                    @foreach($data_traveller as $Traveller)
                    <tr>
                      <td><a href="/admin/traveller/{{$Traveller->id}}">{{strtoupper($Traveller->lname)}}, {{strtoupper($Traveller->fname)}} {{strtoupper($Traveller->mname)}}</a></td>
        
                      <td>
                          <a href="/admin/traveller/{{$Traveller->id}}" class="btn btn-info btn-small"><i class="btn-icon-only fas fa-search"> </i></a>
                      </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
            </div>
          <div class="col-md-9">
            @if(Session::has('success'))
                <div class="alert alert-success">
                    {{ Session::get('success') }}
                    @php
                    Session::forget('success');
                    @endphp
                </div>
            @endif
            @if(Session::has('error'))
                <div class="alert alert-warning">
                    {{ Session::get('error') }}
                    @php
                    Session::forget('error');
                    @endphp
                </div>
            @endif
            <form action="{{route ('add_pass') }}" method="POST">
              @csrf
              <div class="card">
                <div class="card-header">
                  <h3 class="card-title">Add Pass</h3>
                </div>
                <div class="card-body">
                  <div class="row">
                    <div class="col-md-3">
                      <div class="form-group">
                        <label for="First Name">First Name</label>
                        <input type="text" class="form-control" id="fname" name="fname" placeholder="First Name" value="{{ old('fname') }}" required>
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="form-group">
                        <label for="Middle Name">Middle Name</label>
                        <input type="text" class="form-control" id="mname" name="mname" placeholder="Middle Name" value="{{ old('mname') }}">
                      </div>
                      
                    </div>
                    <div class="col-md-3">
                      <div class="form-group">
                        <label for="Last Name">Last Name</label>
                        <input type="text" class="form-control" id="lname" name="lname" placeholder="Last Name" value="{{ old('lname') }}" required>
                      </div>
                    </div>
                    <div class="col-md-3">
                    <div class="form-group">
                      <label for="Purpose">Purpose</label>
                      <input type="text" class="form-control" id="purpose" name="purpose" placeholder="Purpose" required>
                    </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="Destination">Destination</label>
                            <input type="text" class="form-control" id="destination" name="destination" placeholder="Destination" value="{{ old('contact_num') }}" required>
                        </div>
                    </div>
                    <div class="col-md-3">
                      <div class="form-group">
                        <label for="Pass Type">Pass Type</label>
                        <div class="form-group">
                         
                            <select class="form-control" name="pass_type">
                                <option>Travel Pass</option>
                                <option>Special Working Pass</option>
                            </select>
                        </div>
                      </div>
                    </div>
                    <?php 
                    $month = date('m');
                    $day = date('d');
                    $year = date('Y');

                    $today = $year . '-' . $month . '-' . $day;
                    ?>
                    <div class="col-md-3">
                      <div class="form-group">
                        <label for="Validity">Validity</label>
                        <input type="date" class="form-control" id="from" name="from"  value="<?php echo $today;?>" required>
                       
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="form-group">
                        <label for="Validity">&nbsp;</label>
                        
                        <input type="date" class="form-control" id="to" name="to"  value="<?php echo $today;?>" required>
                      </div>
                    </div>
                  </div>
                  
                  <button type="submit" class="btn btn-md btn-info"> <i class="fas fa-save"></i> Save</button>
                </div>
            </form>
            </div>
          </div>
        <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <div class="row">
                    <div class="col-md-6"><h3 class="card-title">Pass List</h3></div>
                    
                </div>
               
              </div>
              <!-- /.card-header -->
              <div class="card-body p-0">
                <br>
                {{$data_pass->links()}}
                <table class="table table-striped">
                  <thead>
                    <tr>
                      
                      <th>Full Name</th>
                      <th>Purpose</th>
                      <th>Destination</th>
                      <th>Validity</th>
                      <th>Pass Type</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($data_pass as $Pass)
                    <tr>
                      <td><a href="/admin/traveller/{{$Pass->traveller->id}}">{{strtoupper($Pass->traveller->lname)}}, {{strtoupper($Pass->traveller->fname)}} {{strtoupper($Pass->traveller->mname)}}</a></td>
                      <td>{{$Pass->purpose}}</td>
                      <td>{{$Pass->destination}}</td>
                      <td>{{$Pass->valid_date_from}} - {{$Pass->valid_date_to}}</td>
                      <td>{{$Pass->pass_type}}</td>
                     
                      <td>
                          <a href="/admin/traveller/{{$Traveller->id}}" class="btn btn-info btn-small"><i class="btn-icon-only fas fa-search"> </i></a>
                      </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div>
        <!-- /.row -->
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>

<script type="text/javascript">
$('#search').on('keyup',function(){
  $value=$(this).val();
  $.ajax({
    type : 'get',
    url : '{{URL::to('admin/pass_search')}}',
    data:{'search':$value},
    success:function(data){
      $('.passresult').html(data);
    } 
  });
})
</script> 
<script type="text/javascript">
$.ajaxSetup({ headers: { 'csrftoken' : '{{ csrf_token() }}' } });
</script>
<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/member.js') }}"></script>
@endsection