@extends('layout.admin')

@section('content')
<div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Traveller</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Traveller</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">

        <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Edit travel record for {{$data_pass->traveller->lname}}, {{$data_pass->traveller->fname}} {{$data_pass->traveller->mname}}</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
              @if(Session::has('success'))
                  <div class="alert alert-success">
                      {{ Session::get('success') }}
                      @php
                      Session::forget('success');
                      @endphp
                  </div>
              @endif
              <form action="{{route ('update_traveller') }}" method="POST">
              @csrf
              <div class="card">
                <div class="card-header">
                  <h3 class="card-title">Update Traveller</h3>
                </div>
                <div class="card-body">
                  <div class="row">
                    <div class="col-md-3">
                      <div class="form-group">
                        <label for="First Name">First Name</label>
                        <input type="text" class="form-control" id="fname" name="fname" placeholder="First Name" value="{{$data_pass->traveller->fname}}" required>
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="form-group">
                        <label for="Middle Name">Middle Name</label>
                        <input type="text" class="form-control" id="mname" name="mname" placeholder="Middle Name" value="{{$data_pass->traveller->mname}}">
                      </div>
                      
                    </div>
                    <div class="col-md-3">
                      <div class="form-group">
                        <label for="Last Name">Last Name</label>
                        <input type="text" class="form-control" id="lname" name="lname" placeholder="Last Name" value="{{$data_pass->traveller->lname}}" required>
                      </div>
                    </div>
                    <div class="col-md-3">
                    <div class="form-group">
                      <label for="Purpose">Purpose</label>
                      <input type="text" class="form-control" id="purpose" name="purpose" placeholder="Purpose" value="{{$data_pass->purpose}}" required>
                    </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="Destination">Destination</label>
                            <input type="text" class="form-control" id="destination" name="destination" placeholder="Destination" value="{{$data_pass->destination}}" required>
                        </div>
                    </div>
                    <div class="col-md-3">
                      <div class="form-group">
                        <label for="Pass Type">Pass Type</label>
                        <div class="form-group">
                         
                            <select class="form-control" name="pass_type">
                                <option>{{$data_pass->pass_type}}</option>
                                <option>Travel Pass</option>
                                <option>Special Working Pass</option>
                            </select>
                        </div>
                      </div>
                    </div>
                   
                    <div class="col-md-3">
                      <div class="form-group">
                        <label for="Validity">Validity</label>
                        <input type="text" class="form-control" id="from" name="from"  value="{{$data_pass->valid_date_from}}" required>
                       
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="form-group">
                        <label for="Validity">&nbsp;</label>
                        
                        <input type="text" class="form-control" id="to" name="to"  value="{{$data_pass->valid_date_to}}" required>
                      </div>
                    </div>
                  </div>
                  <input type="hidden" name="pass_id" value="{{$data_pass->id}}">
                  <input type="hidden" name="travel_id" value="{{$data_pass->traveller->id}}">
                  <button type="submit" class="btn btn-md btn-info"> <i class="fas fa-save"></i> Save</button>
                </div>
            </form>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div>
        <!-- /.row -->
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
@endsection