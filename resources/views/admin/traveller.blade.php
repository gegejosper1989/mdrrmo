@extends('layout.admin')

@section('content')
<div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Traveller</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Traveller</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">

        <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Add travel record for {{$data_traveller->lname}}, {{$data_traveller->fname}} {{$data_traveller->mname}}</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
              @if(Session::has('success'))
                  <div class="alert alert-success">
                      {{ Session::get('success') }}
                      @php
                      Session::forget('success');
                      @endphp
                  </div>
              @endif
                <form action="{{route('add_traveller_pass')}}" method="post">
                  @csrf
                  <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                        <label for="Purpose">Purpose</label>
                        <input type="text" class="form-control" id="purpose" name="purpose" placeholder="Purpose" required>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="Destination">Destination</label>
                            <input type="text" class="form-control" id="destination" name="destination" placeholder="Destination" value="{{ old('contact_num') }}" required>
                        </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label for="Pass Type">Pass Type</label>
                        <div class="form-group">
                         
                            <select class="form-control" name="pass_type">
                                <option>Travel Pass</option>
                                <option>Special Working Pass</option>
                                
                            </select>
                        </div>
                      </div>
                    </div>
                    <?php 
                    $month = date('m');
                    $day = date('d');
                    $year = date('Y');

                    $today = $year . '-' . $month . '-' . $day;
                    ?>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label for="Validity">Validity</label>
                        <input type="date" class="form-control" id="from" name="from"  value="<?php echo $today;?>" required>
                       
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label for="Validity">&nbsp;</label>
                        
                        <input type="date" class="form-control" id="to" name="to"  value="<?php echo $today;?>" required>
                      </div>
                    </div>
                  </div>
                  <input type="hidden" name="traveller_id" value="{{$data_traveller->id}}">
                  <button class="btn btn-sm btn-info" type="submit">Save</button>
                </form>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div>
        <!-- /.row -->
        </div>
        <div class="row">
        <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <div class="row">
                    <div class="col-md-6"><h3 class="card-title">Travel History</h3></div>
                    
                </div>
               
              </div>
              <!-- /.card-header -->
              <div class="card-body p-0">
                <br>
                
                <table class="table table-striped">
                  <thead>
                    <tr>
                      
                      <th>Full Name</th>
                      <th>Purpose</th>
                      <th>Destination</th>
                      <th>Validity</th>
                      <th>Pass Type</th>
                      <th>Status</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($data_traveller->pass as $Pass)
                    <tr>
                      <td><a href="/admin/traveller/{{$data_traveller->id}}">{{strtoupper($Pass->traveller->lname)}}, {{strtoupper($Pass->traveller->fname)}} {{strtoupper($Pass->traveller->mname)}}</a></td>
                      <td>{{$Pass->purpose}}</td>
                      <td>{{$Pass->destination}}</td>
                      <td>{{$Pass->valid_date_from}} - {{$Pass->valid_date_to}}</td>
                      <td>{{$Pass->pass_type}}</td>
                      <td>
                      <?php $today = date('Y-m-d')?>
                        @if($Pass->valid_date_to >= $today)
                            Valid
                        @else
                            Invalid
                        @endif
                      </td>
                      <td>
                      <a href="/admin/traveller/edit/{{$Pass->id}}" class="btn btn-info"><i class="fas fa-edit"></i></a>
                      </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div>
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
@endsection