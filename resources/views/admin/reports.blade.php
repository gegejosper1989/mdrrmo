@extends('layout.admin')

@section('content')
<script src="{{ asset('js/jquery.min.js') }}"></script>
<div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Report</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Report</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <div class="row">
                    <div class="col-md-6"><h3 class="card-title">Pass List</h3></div>
                    
                </div>
               
              </div>
              <!-- /.card-header -->
              <div class="card-body p-0">
                <br>
                {{$data_pass->links()}}
                <form action="{{route ('report_range') }}" method="POST">
                  @csrf
                  <div class="card">
                    <div class="card-header">
                      <h3 class="card-title">Filter Report - {{$data_pass_count}}</h3>
                    </div>
                    <div class="card-body">
                      <div class="row">
                        <div class="col-md-3">
                          <div class="form-group">
                            <label for="Date Range">Date Range</label>
                            <input type="date" class="form-control" id="from" name="from"  required>
                          
                          </div>
                        </div>
                        <div class="col-md-3">
                          <div class="form-group">
                            <label for="Date Range">&nbsp;</label>
                            
                            <input type="date" class="form-control" id="to" name="to" required>
                          </div>
                        </div>
                      </div>
                      
                      <button type="submit" class="btn btn-md btn-info"> <i class="fas fa-save"></i> Filter</button>
                    </div>
                </form>
                <table class="table table-striped">
                  <thead>
                    <tr>
                      
                      <th>Full Name</th>
                      <th>Purpose</th>
                      <th width="150">Destination</th>
                      <th>Validity</th>
                      <th>Pass Type</th>
                      
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($data_pass as $Pass)
                    <tr>
                      <td>{{strtoupper($Pass->traveller->lname)}}, {{strtoupper($Pass->traveller->fname)}} {{strtoupper($Pass->traveller->mname)}}</td>
                      <td>{{$Pass->purpose}}</td>
                      <td>{{$Pass->destination}}</td>
                      <td>{{$Pass->valid_date_from}} - {{$Pass->valid_date_to}}</td>
                      <td>{{$Pass->pass_type}}</td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div>
        <!-- /.row -->
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>

<script type="text/javascript">
$('#search').on('keyup',function(){
  $value=$(this).val();
  $.ajax({
    type : 'get',
    url : '{{URL::to('admin/pass_search')}}',
    data:{'search':$value},
    success:function(data){
      $('.passresult').html(data);
    } 
  });
})
</script> 
<script type="text/javascript">
$.ajaxSetup({ headers: { 'csrftoken' : '{{ csrf_token() }}' } });
</script>
<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/member.js') }}"></script>
@endsection