-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Mar 18, 2020 at 03:48 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.1.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `covid_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `brgyusers`
--

CREATE TABLE `brgyusers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `brgy` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `brgyusers`
--

INSERT INTO `brgyusers` (`id`, `user_id`, `brgy`, `created_at`, `updated_at`) VALUES
(3, '6', 'BARONGCOT', '2020-03-18 05:57:01', '2020-03-18 05:57:01');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2020_03_18_005717_create_patients_table', 1),
(4, '2020_03_18_131407_create_brgyusers_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `patients`
--

CREATE TABLE `patients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `fname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mname` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `age` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gender` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_num` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nationality` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `brgy` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_exposure` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `travel_history` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `symptoms` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `patient_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `patients`
--

INSERT INTO `patients` (`id`, `fname`, `lname`, `mname`, `age`, `gender`, `contact_num`, `nationality`, `address`, `brgy`, `date_exposure`, `travel_history`, `symptoms`, `patient_type`, `created_at`, `updated_at`) VALUES
(1, 'EMELY', 'LANGERAS', 'N/A', '26', 'F', '0909-000-0000', 'FILIPINO', 'SAPA LUBOC, AZDS', 'SAPA LUBOC', '09/03/2020', ' KUWAIT', 'N/A', 'PUM', NULL, NULL),
(2, 'RAUL', 'ESTENZO', 'R', '49', 'M', '0909-000-0001', 'FILIPINO', 'SAPA LUBOC, AZDS', 'SAPA LUBOC', '12/03/2020', ' BATANGAS', 'N/A', 'PUM', NULL, NULL),
(3, 'IRISH', 'BONTUYAN', 'N/A', '22', 'F', '0909-000-0002', 'FILIPINO', 'SAPA LUBOC, AZDS', 'SAPA LUBOC', '13/03/2020', ' PARA?AQUE', 'N/A', 'PUM', NULL, NULL),
(4, 'AIMEE', 'ARCILLA', 'N/A', '25', 'F', '0909-000-0003', 'FILIPINO', 'SAPA LUBOC, AZDS', 'SAPA LUBOC', '13/03/2020', ' PARA?AQUE', 'N/A', 'PUM', NULL, NULL),
(5, 'NIXON', 'ARIAS', 'B', '48', 'M', '0909-000-0004', 'FILIPINO', 'BALINTAWAK, AZDS', 'BALINTAWAK', '13/03/2020', ' ILO-ILO', 'N/A', 'PUM', NULL, NULL),
(6, 'ROSITA', 'REPULLE', 'N/A', '59', 'F', '0909-000-0005', 'FILIPINO', 'BALINTAWAK, AZDS', 'BALINTAWAK', '13/03/2020', ' ILO-ILO', 'N/A', 'PUM', NULL, NULL),
(7, 'IKE KENN', 'BAJAO', 'N/A', '26', 'M', '0909-000-0006', 'FILIPINO', 'BALINTAWAK, AZDS', 'BALINTAWAK', '14/03/2020', ' ILO-ILO', 'N/A', 'PUM', NULL, NULL),
(8, 'VERGIE', 'CANTILA', 'N/A', '50', 'F', '0909-000-0007', 'FILIPINO', 'BAKI, AZDS', 'BAKI', '14/03/2020', 'ILO-ILO', 'N/A', 'PUM', NULL, NULL),
(9, 'JENNY', 'VILLARIZA', 'N/A', '42', 'F', '0909-000-0008', 'FILIPINO', 'BAKI, AZDS', 'BAKI', '14/03/2020', ' ILO-ILO', 'N/A', 'PUM', NULL, NULL),
(10, 'JADE', 'VALLEJO', 'F', '37', 'M', '0909-000-0009', 'FILIPINO', 'SAN JUAN, AZDS', 'SAN JUAN', '16/03/2020', 'MANILA-BOHOL-CEBU', 'N/A', 'PUM', NULL, NULL),
(11, 'JOSHIEN AIFE', 'DURENS', 'N/A', '21', 'F', '0909-000-0010', 'FILIPINO', 'LANTUNGAN, AZDS', 'LANTUNGAN', '11/03/2020', 'MANILA', 'N/A', 'PUM', NULL, NULL),
(12, 'AR-KACHRINE', 'GERUNDIO', 'N/A', '21', 'F', '0909-000-0011', 'FILIPINO', 'LANTUNGAN, AZDS', 'LANTUNGAN', '16/03/2020', 'CDO', 'N/A', 'PUM', NULL, NULL),
(13, 'BABY JEAN', 'ORO', 'M', '18', 'F', '0909-000-0012', 'FILIPINO', 'COMMONWEALTH, AZDS', 'COMMONWEALTH', '13/03/2020', ' TUGUEGARAO', 'N/A', 'PUM', NULL, NULL),
(14, 'MYZEL', 'LUNA', 'N/A', '40', 'F', '0909-000-0013', 'FILIPINO', 'COMMONWEALTH, AZDS', 'COMMONWEALTH', '14/03/2020', ' ILO-ILO', 'N/A', 'PUM', NULL, NULL),
(15, 'CHRISTOPHER', 'ORILLE', 'N/A', '52', 'M', '0909-000-0014', 'FILIPINO', 'COMMONWEALTH, AZDS', 'COMMONWEALTH', '14/03/2020', 'DAVAO', 'N/A', 'PUM', NULL, NULL),
(16, 'GHEVE', 'ARCONADO', 'N/A', '32', 'F', '0909-000-0015', 'FILIPINO', 'COMMONWEALTH, AZDS', 'COMMONWEALTH', '14/03/2020', ' QUEZON CITY', 'N/A', 'PUM', NULL, NULL),
(17, 'OLIVER', 'VELASCO', 'N/A', '37', 'M', '0909-000-0016', 'FILIPINO', 'PANAGHIUSA, AZDS', 'PANAGHIUSA', '14/03/2020', 'ILO-ILO', 'N/A', 'PUM', NULL, NULL),
(18, ' AGNES', 'RUILES', 'N/A', '35', 'F', '0909-000-0017', 'FILIPINO', 'PANAGHIUSA, AZDS', 'PANAGHIUSA', '14/03/2020', ' ILO-ILO', 'N/A', 'PUM', NULL, NULL),
(19, 'VICTORIA', 'LABANGON', 'N/A', '68', 'F', '0909-000-0018', 'FILIPINO', 'K. WEST, AZDS', 'KAHAYAGAN WEST', '14/03/2020', ' ILO-ILO', 'N/A', 'PUM', NULL, NULL),
(20, 'LENIE', 'BORRES', 'N/A', '32', 'F', '0909-000-0019', 'FILIPINO', 'K. WEST, AZDS', 'KAHAYAGAN WEST', '14/03/2020', ' ILO-ILO', 'N/A', 'PUM', NULL, NULL),
(21, 'RHEA', 'ARVELO', 'N/A', '28', 'F', '0909-000-0020', 'FILIPINO', 'K. WEST, AZDS', 'KAHAYAGAN WEST', '14/03/2020', ' ILO-ILO', 'N/A', 'PUM', NULL, NULL),
(22, 'CAMELO', 'TABURADA', 'N/A', '45', 'M', '0909-000-0021', 'FILIPINO', 'K. WEST, AZDS', 'KAHAYAGAN WEST', '14/03/2020', ' MANILA', 'N/A', 'PUM', NULL, NULL),
(23, 'SANFORD', 'VLINDA', 'N/A', 'N/A', 'F', '0909-000-0022', 'FILIPINO', 'K. EAST, AZDS', 'KAHAYAGAN WEST', '13/03/2020', ' NEW ZEALAND', 'N/A', 'PUM', NULL, NULL),
(24, 'JEAN', 'CENIZA', 'N/A', '18', 'F', '0909-000-0023', 'FILIPINO', 'K. EAST, AZDS', 'KAHAYAGAN WEST', '15/03/2020', ' ILIGAN', 'N/A', 'PUM', NULL, NULL),
(25, 'MA. MILAGROS', 'TIGUE', 'N/A', '52', 'F', '0909-000-0024', 'FILIPINO', 'K. EAST, AZDS', 'KAHAYAGAN WEST', '16/03/2020', 'CEBU CITY', 'N/A', 'PUM', NULL, NULL),
(26, 'JOHN DAREN', 'TIGUE', 'N/A', '25', 'M', '0909-000-0025', 'FILIPINO', 'K. EAST, AZDS', 'KAHAYAGAN WEST', '16/03/2020', ' CEBU CITY', 'N/A', 'PUM', NULL, NULL),
(27, 'CHYRIN', 'TIGUE', 'N/A', '22', 'F', '0909-000-0026', 'FILIPINO', 'K. EAST, AZDS', 'KAHAYAGAN WEST', '16/03/2020', ' CEBU CITY', 'N/A', 'PUM', NULL, NULL),
(28, 'KRISTIAN', 'TIGUE', 'N/A', '19', 'M', '0909-000-0027', 'FILIPINO', 'K. EAST, AZDS', 'KAHAYAGAN WEST', '16/03/2020', ' CEBU CITY', 'N/A', 'PUM', NULL, NULL),
(29, 'RYLE JUHENCE', 'TIGUE', 'N/A', '2', 'M', '0909-000-0028', 'FILIPINO', 'K. EAST, AZDS', 'KAHAYAGAN WEST', '16/03/2020', ' CEBU CITY', 'N/A', 'PUM', NULL, NULL),
(30, ' NENITA', 'ALIVIO', 'N/A', '53', 'F', '0909-000-0029', 'FILIPINO', 'CEBUNEG, AZDS', 'CEBUNEG', '11/03/2020', ' PASIG CITY (MANILA)', 'N/A', 'PUM', NULL, NULL),
(31, 'NARCISA', 'TALISIC', 'N/A', '57', 'F', '0909-000-0030', 'FILIPINO', 'CAMPO UNO, ZDS', 'CAMPO UNO', '13/03/2020', ' ILO-ILO', 'N/A', 'PUM', NULL, NULL),
(32, 'REZALINE', 'BACUS', 'N/A', '43', 'F', '0909-000-0031', 'FILIPINO', 'CAMPO UNO, ZDS', 'CAMPO UNO', '13/03/2020', ' ILO-ILO', 'N/A', 'PUM', NULL, NULL),
(33, 'RICKY', 'ALIA', 'N/A', '36', 'M', '0909-000-0032', 'FILIPINO', 'CAMPO UNO, ZDS', 'CAMPO UNO', '15/03/2020', ' ILO-ILO', 'N/A', 'PUM', NULL, NULL),
(34, 'MARKEL', 'REFORMADO', 'N/A', '35', 'M', '0909-000-0033', 'FILIPINO', 'BEMPOSA, AZDS', 'BEMPOSA', '15/03/2020', ' CDO', 'N/A', 'PUM', NULL, NULL),
(35, 'JOMEL IWAY', 'ARMODIA', 'N/A', '22', 'M', '0909-000-0034', 'FILIPINO', 'BEMPOSA, AZDS', 'BEMPOSA', '15/03/2020', ' CDO', 'N/A', 'PUM', NULL, NULL),
(36, 'JOSE', 'CASAS', 'N/A', '58', 'M', '0909-000-0035', 'FILIPINO', 'B. PITOGO, AZDS', 'B. PITOGO', '13/03/2020', ' ILO-ILO', 'N/A', 'PUM', NULL, NULL),
(38, 'JEANNE', 'SARIP', 'N/A', 'N/A', 'F', '0909-000-0037', 'FILIPINO', 'BALAS, AZDS', 'BALAS', '12/03/2020', ' CDO', 'N/A', 'PUM', NULL, NULL),
(39, 'DAVID', 'BATERSAL', 'N/A', '21', 'M', '0909-000-0038', 'FILIPINO', 'BALAS, AZDS', 'BALAS', '16/03/2020', 'CDO', 'N/A', 'PUM', NULL, NULL),
(40, 'ARVIN', 'ESTRERA', 'N/A', '41', 'M', '0909-000-0039', 'FILIPINO', 'ALEGRIA, AZDS', 'ALEGRIA', '15/03/2020', ' CEBU', 'N/A', 'PUM', NULL, NULL),
(41, 'WENDELYN', 'ROMANILLOS', 'N/A', '36', 'F', '0909-000-0040', 'FILIPINO', 'LA PAZ, AZDS', 'LA PAZ', '15/03/2020', ' MANILA', 'N/A', 'PUM', NULL, NULL),
(42, 'JEAN', 'MARTINEZ', 'N/A', 'N/A', 'N/A', '0909-000-0041', 'FILIPINO', 'LA PAZ, AZDS', 'LA PAZ', '11/03/2020', ' KUWAIT', 'N/A', 'PUM', NULL, NULL),
(43, 'AILYN', 'MANCO', 'N/A', '28', 'F', '0909-000-0042', 'FILIPINO', 'LA PAZ, AZDS', 'LA PAZ', '15/03/2020', ' DAVAO', 'N/A', 'PUM', NULL, NULL),
(44, 'EMMA BELEN', 'MAKIG-ANGAY', 'N/A', '52', 'F', '0909-000-0043', 'FILIPINO', 'LINTUGOP, AZDS', 'LINTUGOP', '14/03/2020', ' ILO-ILO', 'N/A', 'PUM', NULL, NULL),
(45, 'GUILLENA', 'TUDTUD', 'N/A', '53', 'F', '0909-000-0044', 'FILIPINO', 'LINTUGOP, AZDS', 'LINTUGOP', '14/03/2020', ' ILO-ILO', 'N/A', 'PUM', NULL, NULL),
(46, 'HELEN', 'STAMINA', 'N/A', '42', 'F', '0909-000-0045', 'FILIPINO', 'LINTUGOP, AZDS', 'LINTUGOP', '14/03/2020', ' ILO-ILO', 'N/A', 'PUM', NULL, NULL),
(47, 'KENTH JOY', 'CONEJOS', 'N/A', '20', 'F', '0909-000-0046', 'FILIPINO', ' LINTUGOP, AZDS', 'LINTUGOP', '16/03/2020', ' CEBU', 'N/A', 'PUM', NULL, NULL),
(48, 'CLIFFORD', 'CUENCO', 'N/A', '22', 'M', '0909-000-0047', 'FILIPINO', 'LINTUGOP, AZDS', 'LINTUGOP', '16/03/2020', ' CAVITE', 'N/A', 'PUM', NULL, NULL),
(49, 'FELY', 'TUBIANO', 'N/A', '34', 'F', '0909-000-0048', 'FILIPINO', 'LA VICTORIA, AZDS', 'LA VICTORIA', '08/03/2020', ' ', 'N/A', 'PUM', NULL, NULL),
(50, 'JENNIFER', 'JAVIER', 'N/A', '24', 'F', '0909-000-0049', 'FILIPINO', 'P. 2, ANONANG, AZDS', 'ANONANG', '13/03/2020', ' MANILA-AURORA', 'NONE', 'PUM', NULL, NULL),
(51, 'ELMAR', 'CANDIA', 'N/A', '24', 'M', '0909-000-0050', 'FILIPINO', 'P. 2, ANONANG, AZDS', 'ANONANG', ' 3/2/2020', ' BATANGAS-DUMAGUETE-AURORA', 'NONE', 'PUM', NULL, NULL),
(52, 'JONNIE', 'AMACA', 'N/A', '35', 'M', '0909-000-0051', 'FILIPINO', 'ANONANG, AZDS', 'ANONANG', '04/03/2020', ' BATANGAS-DUMAGUETE-AURORA ', 'NONE', 'PUM', NULL, NULL),
(53, 'ROSALIE MUNOZ', 'GENOBISA', 'N/A', 'N/A', 'F', '0909-000-0052', 'FILIPINO', 'ANONANG, AZDS', 'ANONANG', '12/03/2020', ' ILO-ILO', 'NONE', 'PUM', NULL, NULL),
(54, 'BEN JAMES', 'RUIZ', 'N/A', 'N/A', 'M', '0909-000-0053', 'FILIPINO', 'ANONANG, AZDS', 'ANONANG', '13/03/2020', ' ILO-ILO', 'NONE', 'PUM', NULL, NULL),
(55, 'EDGAR', 'PILAPIL', 'N/A', 'N/A', 'M', '0909-000-0054', 'FILIPINO', 'GUBAAN', 'GUBAAN', '12/03/2020', ' ILO ILO', 'N/A', 'PUM', NULL, NULL),
(56, 'AYING', 'PILAPIL', 'N/A', 'N/A', 'F', '0909-000-0055', 'FILIPINO', 'GUBAAN', 'GUBAAN', '12/03/2020', ' ILO ILO', 'N/A', 'PUM', NULL, NULL),
(57, 'ARCHIVAL', 'SHERBY', 'N/A', '27', 'F', '0909-000-0056', 'FILIPINO', 'GUBAAN', 'GUBAAN', '15/03/2020', ' COTABATO', 'N/A', 'PUM', NULL, NULL),
(58, 'PEPITO', 'JANIVIE', 'N/A', '28', 'F', '0909-000-0057', 'FILIPINO', 'GUBAAN', 'GUBAAN', '14/03/2020', ' ILO-ILO', 'N/A', 'PUM', NULL, NULL),
(59, 'PRINCESS HONNY', 'MAMAWE', 'N/A', '10', 'F', '0909-000-0058', 'FILIPINO', 'GUBAAN', 'GUBAAN', '13/03/2020', ' TUGUEGARAO', 'N/A', 'PUM', NULL, NULL),
(60, 'QUINO', 'MARISSA', 'N/A', '44', 'F', '0909-000-0059', 'FILIPINO', 'B. MANDAUE, AZDS', 'BAGONG MANDAUE', '13/03/2020', ' ILO-ILO', 'N/A', 'PUM', NULL, NULL),
(61, 'MANGUYANON', 'NAOMI', 'N/A', '47', 'F', '0909-000-0060', 'FILIPINO', 'B. MANDAUE, AZDS', 'BAGONG MANDAUE', '13/03/2020', ' ILO-ILO', 'N/A', 'PUM', NULL, NULL),
(62, 'JUBAN', 'LLOYD', 'N/A', '20', 'M', '0909-000-0061', 'FILIPINO', 'B. MANDAUE, AZDS', 'BAGONG MANDAUE', '13/03/2020', ' ILO-ILO', 'N/A', 'PUM', NULL, NULL),
(63, 'PINO', 'RODRIGO', 'N/A', '55', 'M', '0909-000-0062', 'FILIPINO', 'B. MANDAUE, AZDS', 'BAGONG MANDAUE', '13/03/2020', ' ILO-ILO', 'N/A', 'PUM', NULL, NULL),
(64, 'WALTER', 'BONJOC', 'N/A', 'N/A', 'M', '0909-000-0063', 'FILIPINO', 'SAN FRANCISCO, BALIDE, AZDS', 'BALIDE', '3/11/2020', ' MANILA', 'N/A', 'PUM', NULL, NULL),
(65, 'ANN-ANN', 'BONJOC', 'N/A', 'N/A', 'F', '0909-000-0064', 'FILIPINO', 'SAN FRANCISCO, BALIDE, AZDS', 'BALIDE', '3/11/2020', ' MANILA', 'N/A', 'PUM', NULL, NULL),
(66, 'JOSIAH', 'VIVERO', 'N/A', '20', 'F', '0909-000-0065', 'FILIPINO', 'BALIDE, AZDS', 'BALIDE', '16/03/2020', 'CEBU', 'N/A', 'PUM', NULL, NULL),
(67, 'VINCE ACCEL', 'MONTALBAN', 'N/A', '11', 'M', '0909-000-0066', 'FILIPINO', 'BALIDE, AZDS', 'BALIDE', '13/03/2020', ' TUGUEGARAO', 'N/A', 'PUM', NULL, NULL),
(68, 'LILY MAE', 'MONTALBAN', 'N/A', '38', 'F', '0909-000-0067', 'FILIPINO', 'BALIDE, AZDS', 'BALIDE', '13/03/2020', ' TUGUEGARAO', 'N/A', 'PUM', NULL, NULL),
(69, 'MA. VITA', 'BASIGA', 'N/A', '55', 'F', '0909-000-0068', 'FILIPINO', 'ALAY KAPWA, BALIDE, AZDS', 'BALIDE', '15/03/2020', ' ANTIPOLO', 'N/A', 'PUM', NULL, NULL),
(70, 'AMADO', 'BASIGA', 'N/A', '58', 'M', '0909-000-0069', 'FILIPINO', 'ALAY KAPWA, BALIDE, AZDS', 'BALIDE', '15/03/2020', ' ANTIPOLO', 'N/A', 'PUM', NULL, NULL),
(71, 'Nikki Valerie', ' Cobarrubias ', 'N/A', '11', 'F', '0909-000-0070', 'Filipino', 'Monte Alegre, Aurroa,ZDS', 'Monte Alegre', '11/03/2020', ' Baguio City', 'March 11,2020(FEVER, BODY MALAISE, HEADACHE)', 'PUM', NULL, NULL),
(72, 'GINA', 'PANILAGAN', 'N/A', 'N/A', 'F', '0909-000-0071', 'Filipino', 'Monte Alegre, Aurroa,ZDS', 'Monte Alegre', '12/03/2020', ' BAGUIO CITY', 'N/A', 'PUM', NULL, NULL),
(73, 'ANABELLE', 'DAGANAN', 'N/A', '28', 'F', '0909-000-0072', 'Filipino', 'Monte Alegre, Aurroa,ZDS', 'Monte Alegre', '13/03/2020', ' DUMAGUETE', 'N/A', 'PUM', NULL, NULL),
(74, 'CLARINA', 'ELMEDULAN', 'N/A', '38', 'F', '0909-000-0073', 'Filipino', 'Monte Alegre, Aurroa,ZDS', 'Monte Alegre', '14/03/2020', ' TUGUEGARAO', 'N/A', 'PUM', NULL, NULL),
(75, 'FRANCIS JOHN', 'GAMBITAN', 'N/A', '19', 'M', '0909-000-0074', 'Filipino', 'Monte Alegre, Aurroa,ZDS', 'Monte Alegre', '14/03/2020', ' PALAWAN', 'N/A', 'PUM', NULL, NULL),
(76, 'JOCELYN', 'MANALIM', 'N/A', '50', 'F', '0909-000-0075', 'Filipino', 'Monte Alegre, Aurroa,ZDS', 'Monte Alegre', '14/03/2020', ' ILO-ILO', 'N/A', 'PUM', NULL, NULL),
(77, 'JOCELYN', 'MAGDADARO', 'N/A', '43', 'F', '0909-000-0076', 'Filipino', 'Monte Alegre, Aurroa,ZDS', 'Monte Alegre', '14/03/2020', ' ILO-ILO', 'N/A', 'PUM', NULL, NULL),
(78, 'SOFRONIA', 'BONTILAO', 'N/A', '68', 'F', '0909-000-0077', 'Filipino', 'Monte Alegre, Aurroa,ZDS', 'Monte Alegre', '14/03/2020', ' ILO-ILO', 'N/A', 'PUM', NULL, NULL),
(79, 'MERLYN', 'BUGTAY', 'N/A', '45', 'F', '0909-000-0078', 'Filipino', 'Monte Alegre, Aurroa,ZDS', 'Monte Alegre', '14/03/2020', ' ILO-ILO', 'N/A', 'PUM', NULL, NULL),
(80, 'Ditas', 'Pareja', 'N/A', '45', 'F', '0909-000-0079', 'Filipino', 'Romarate, Aurora, ZDS', 'Romarate', '11/03/2020', ' Baguio City', 'N/A', 'PUM', NULL, NULL),
(81, ' Princess Noelle', ' Pareja', 'N/A', '6', 'F', '0909-000-0080', 'Filipino', 'Romarate, Aurora, ZDS', 'Romarate', '11/03/2020', ' Baguio City', 'N/A', 'PUM', NULL, NULL),
(82, 'ERNAN BOY', 'TORRES', 'N/A', '23', 'M', '0909-000-0081', 'Filipino', 'Romarate, Aurora, ZDS', 'Romarate', '12/03/2020', ' THAILAND', 'N/A', 'PUM', NULL, NULL),
(83, 'DIANNE ROSE', 'FERNANDEZ', 'N/A', '23', 'F', '0909-000-0082', 'Filipino', 'Romarate, Aurora, ZDS', 'Romarate', '12/03/2020', ' ILIGAN', 'N/A', 'PUM', NULL, NULL),
(84, ' BUSH RANA', 'YUSOP', 'N/A', '19', 'F', '0909-000-0083', 'Filipino', 'Romarate, Aurora, ZDS', 'Romarate', '11/03/2020', ' MARAWI', 'N/A', 'PUM', NULL, NULL),
(85, 'ARNEL', 'MACABABAYAO', 'N/A', 'N/A', 'N/A', '0909-000-0084', 'Filipino', 'Romarate, Aurora, ZDS', 'Romarate', '14/03/2020', 'BATAAN', 'N/A', 'PUM', NULL, NULL),
(86, 'MSGT FEBRICO', 'NERI', 'N/A', '47', 'M', '0909-000-0085', 'Filipino', 'Romarate, Aurora, ZDS', 'Romarate', '08/03/2020', ' PALAWAN', 'N/A', 'PUM', NULL, NULL),
(87, 'JUNNYLE PRAISE', 'SORONIO,', 'N/A', '22', 'F', '0909-000-0086', 'Filipino', 'Romarate, Aurora, ZDS', 'Romarate', '13/03/2020', 'MAKATI', 'N/A', 'PUM', NULL, NULL),
(88, 'DIONESIO', 'SORONIO', 'N/A', '46', 'M', '0909-000-0087', 'Filipino', 'Romarate, Aurora, ZDS', 'Romarate', '13/03/2020', 'MAKATI', 'N/A', 'PUM', NULL, NULL),
(89, 'ZALDY', 'BAYNOSA', 'N/A', '23', 'M', '0909-000-0088', 'Filipino', 'Romarate, Aurora, ZDS', 'Romarate', '13/03/2020', ' MANILA', 'N/A', 'PUM', NULL, NULL),
(90, 'Rosemarie ', 'Indoc', 'N/A', '39', 'F', '0909-000-0089', 'Filipino', 'Libertad, Aurora, ZDS', 'Libertad', '11/03/2020', ' NKTI-Quezon', 'N/A', 'PUM', NULL, NULL),
(91, ' Marian Faith', 'Indoc', 'N/A', '1', 'F', '0909-000-0090', 'Filipino', 'Libertad, Aurora, ZDS', 'Libertad', '11/03/2020', ' NKTI-Quezon', 'N/A', 'PUM', NULL, NULL),
(92, ' SAUL ANDREW', 'TARANZA', 'N/A', '30', 'M', '0909-000-0091', 'Filipino', 'Libertad, Aurora, ZDS', 'Libertad', '15/03/2020', ' AUSTRALIA-MANILA', 'N/A', 'PUM', NULL, NULL),
(93, 'JOHN PAUL', 'AMPARADO', 'N/A', '21', 'M', '0909-000-0092', 'Filipino', 'Libertad, Aurora, ZDS', 'Libertad', '15/03/2020', ' ISABELA', 'N/A', 'PUM', NULL, NULL),
(94, 'GADWIN', 'RUSTATA', 'N/A', '20', 'M', '0909-000-0093', 'Filipino', 'Libertad, Aurora, ZDS', 'Libertad', '16/03/2020', 'CDO', 'N/A', 'PUM', NULL, NULL),
(95, 'GODFFREY', 'RUSTATA', 'N/A', '20', 'M', '0909-000-0094', 'Filipino', 'Libertad, Aurora, ZDS', 'Libertad', '16/03/2020', 'CDO', 'N/A', 'PUM', NULL, NULL),
(96, 'ELMER', 'MERIN', 'N/A', '32', 'M', '0909-000-0095', 'Filipino', 'Libertad, Aurora, ZDS', 'Libertad', '14/03/2020', ' LEYTE-SURIGAO-CDO', 'N/A', 'PUM', NULL, NULL),
(97, 'ROMIE', 'OPORTO', 'N/A', '47', 'M', '0909-000-0096', 'Filipino', 'Libertad, Aurora, ZDS', 'Libertad', '14/03/2020', ' LEYTE-SURIGAO-CDO', 'N/A', 'PUM', NULL, NULL),
(98, ' Elsie', 'Angeles', 'N/A', '42', 'F', '0909-000-0097', 'Filipino', 'San Jose, Aurora, ZDS', 'San Jose', '11/03/2020', ' Baguio City', 'N/A', 'PUM', NULL, NULL),
(99, 'Princess Elise', ' Angeles', 'N/A', '6', 'F', '0909-000-0098', 'Filipino', 'San Jose, Aurora, ZDS', 'San Jose', '11/03/2020', ' Baguio City', 'N/A', 'PUM', NULL, NULL),
(100, 'Dorcas', ' Dacanay', 'N/A', '50', 'F', '0909-000-0099', 'Filipino', 'San Jose, Aurora, ZDS', 'San Jose', '11/03/2020', ' Baguio City', 'N/A', 'PUM', NULL, NULL),
(101, 'Hallie Rhianna', 'Judilla', 'N/A', '9', 'F', '0909-000-0100', 'Filipino', 'San Jose, Aurora, ZDS', 'San Jose', '11/03/2020', ' Baguio City', 'N/A', 'PUM', NULL, NULL),
(102, 'Hannah', 'Judilla', 'N/A', '33', 'F', '0909-000-0101', 'Filipino', 'San Jose, Aurora, ZDS', 'San Jose', '11/03/2020', ' Baguio City', 'N/A', 'PUM', NULL, NULL),
(103, 'Nicelle May', 'Perales', 'N/A', '44', 'F', '0909-000-0102', 'Filipino', 'San Jose, Aurora, ZDS', 'San Jose', '11/03/2020', ' Baguio City', 'N/A', 'PUM', NULL, NULL),
(104, 'SARAH JANE', 'SOLON', 'N/A', '39', 'F', '0909-000-0103', 'Filipino', 'San Jose, Aurora, ZDS', 'San Jose', '14/03/2020', ' ILIGAN', 'N/A', 'PUM', NULL, NULL),
(105, 'JOREY', 'SOLON', 'N/A', '40', 'M', '0909-000-0104', 'Filipino', 'San Jose, Aurora, ZDS', 'San Jose', '14/03/2020', ' ILIGAN', 'N/A', 'PUM', NULL, NULL),
(106, 'SANDARA CHEYENE', 'SOLON,', 'N/A', '15', 'F', '0909-000-0105', 'Filipino', 'San Jose, Aurora, ZDS', 'San Jose', '14/03/2020', ' ILIGAN', 'N/A', 'PUM', NULL, NULL),
(107, 'AARON', 'TRANCO', 'N/A', '57', 'M', '0909-000-0106', 'Filipino', 'San Jose, Aurora, ZDS', 'San Jose', '13/03/2020', ' MANILA', 'N/A', 'PUM', NULL, NULL),
(108, 'JAN RAINER', 'UBAS', 'N/A', '20', 'M', '0909-000-0107', 'Filipino', 'San Jose, Aurora, ZDS', 'San Jose', '15/03/2020', ' CEBU', 'N/A', 'PUM', NULL, NULL),
(109, 'CARL JOSHUA', 'JUBAN', 'N/A', '20', 'M', '0909-000-0108', 'Filipino', 'San Jose, Aurora, ZDS', 'San Jose', '16/03/2020', ' CEBU', 'N/A', 'PUM', NULL, NULL),
(110, 'NESTORA', 'ARSUA', 'N/A', '74', 'F', '0909-000-0109', 'Filipino', 'San Jose, Aurora, ZDS', 'San Jose', '15/03/2020', ' CEBU', 'N/A', 'PUM', NULL, NULL),
(111, 'ELVIE', 'CABILAN', 'N/A', '21', 'F', '0909-000-0110', 'Filipino', 'San Jose, Aurora, ZDS', 'San Jose', '15/03/2020', ' CEBU', 'N/A', 'PUM', NULL, NULL),
(112, 'HEAVEN EJAY', 'MADRANGCA', 'N/A', '20', 'F', '0909-000-0111', 'Filipino', 'San Jose, Aurora, ZDS', 'San Jose', '15/03/2020', 'CEBU', 'N/A', 'PUM', NULL, NULL),
(113, 'REZEL MAE', 'VELASQUEZ', 'N/A', '19', 'F', '0909-000-0112', 'Filipino', 'San Jose, Aurora, ZDS', 'San Jose', '14/03/2020', 'ILIGAN', 'N/A', 'PUM', NULL, NULL),
(114, 'IVORY CARNEL', 'JAYME', 'N/A', '24', 'F', '0909-000-0113', 'Filipino', 'San Jose, Aurora, ZDS', 'San Jose', '13/03/2020', ' TUGUEGARAO', 'N/A', 'PUM', NULL, NULL),
(115, 'NANETTE', 'BUSTILLO', 'N/A', '41', 'F', '0909-000-0114', 'Filipino', 'San Jose, Aurora, ZDS', 'San Jose', '13/03/2020', ' TUGUEGARAO', 'N/A', 'PUM', NULL, NULL),
(116, 'KATZ KARYLL', 'VENTURES', 'N/A', '19', 'F', '0909-000-0115', 'Filipino', 'San Jose, Aurora, ZDS', 'San Jose', '16/03/2020', ' CDO', 'N/A', 'PUM', NULL, NULL),
(117, 'ARIEL', 'ANO-OS, ', 'N/A', '33', 'M', '0909-000-0116', 'Filipino', 'P. ROSAS A, SAN JOSE, AZDS', 'San Jose', '16/03/2020', ' MAN-PAMPANGA,CEBU-AURORA', 'N/A', 'PUM', NULL, NULL),
(118, 'CESARIO', 'JUDILLA', 'N/A', '73', 'M', '0909-000-0117', 'Filipino', 'P. ROSAS A, SAN JOSE, AZDS', 'San Jose', '16/03/2020', ' CEBU', 'N/A', 'PUM', NULL, NULL),
(119, 'CORAZON', 'JUDILLA', 'N/A', '69', 'F', '0909-000-0118', 'Filipino', 'P. ROSAS A, SAN JOSE, AZDS', 'San Jose', '16/03/2020', ' CEBU', 'N/A', 'PUM', NULL, NULL),
(120, 'DEXTER JOSEPH', 'JUDILLA', 'N/A', '44', 'M', '0909-000-0119', 'Filipino', 'P. ROSAS A, SAN JOSE, AZDS', 'San Jose', '16/03/2020', ' CEBU', 'N/A', 'PUM', NULL, NULL),
(121, 'CESARIO', 'JUDILLA', 'N/A', '39', 'M', '0909-000-0120', 'Filipino', 'P. ROSAS A, SAN JOSE, AZDS', 'San Jose', '16/03/2020', 'CEBU', 'N/A', 'PUM', NULL, NULL),
(122, 'JHONA MAY', 'FLORES', 'N/A', '19', 'F', '0909-000-0121', 'FILIPINO', 'SAN JOSE, AZDS', 'San Jose', '16/03/2020', 'CEBU', 'N/A', 'PUM', NULL, NULL),
(123, 'JOHANNA ROSE', 'MATUTES', 'N/A', '20', 'F', '0909-000-0122', 'FILIPINO', 'SAN JOSE, AZDS', 'San Jose', '16/03/2020', 'CEBU', 'N/A', 'PUM', NULL, NULL),
(124, 'Ma. Flordeliza', ' Ardiente', 'N/A', '53', 'F', '0909-000-0123', 'Filipino', 'Poblacion, Aurora, ZDS', 'Poblacion', '11/03/2020', ' Baguio City', 'N/A', 'PUM', NULL, NULL),
(125, ' Lavyrly', ' Lucero', 'N/A', '7', 'F', '0909-000-0124', 'Filipino', 'Poblacion, Aurora, ZDS', 'Poblacion', '11/03/2020', ' Baguio City', 'N/A', 'PUM', NULL, NULL),
(126, 'Yvonne Crystal', 'Lucero', 'N/A', '23', 'F', '0909-000-0125', 'Filipino', 'Poblacion, Aurora, ZDS', 'Poblacion', '11/03/2020', ' Baguio City', 'N/A', 'PUM', NULL, NULL),
(127, 'Carmelline Maize', 'Mosqueda', 'N/A', '7', 'F', '0909-000-0126', 'Filipino', 'Poblacion, Aurora, ZDS', 'Poblacion', '11/03/2020', ' Baguio City', 'N/A', 'PUM', NULL, NULL),
(128, 'Marites', 'Mosqueda', 'N/A', '43', 'F', '0909-000-0127', 'Filipino', 'Poblacion, Aurora, ZDS', 'Poblacion', '11/03/2020', ' Baguio City', 'N/A', 'PUM', NULL, NULL),
(129, 'JUNYL', 'BRAGA', 'N/A', '20', 'M', '0909-000-0128', 'Filipino', 'Poblacion, Aurora, ZDS', 'Poblacion', '15/03/2020', 'CEBU CITY', 'N/A', 'PUM', NULL, NULL),
(130, 'DAME ALTHEA', 'ENRIQUEZ,', 'N/A', '19', 'F', '0909-000-0129', 'Filipino', 'Poblacion, Aurora, ZDS', 'Poblacion', '15/03/2020', 'CEBU CITY', 'N/A', 'PUM', NULL, NULL),
(131, 'DONNA AMHAREL', 'ENRIQUEZ', 'N/A', '21', 'F', '0909-000-0130', 'Filipino', 'Poblacion, Aurora, ZDS', 'Poblacion', '15/03/2020', 'CEBU CITY', 'N/A', 'PUM', NULL, NULL),
(132, 'JOELLYN MAE', 'SISON', 'N/A', '29', 'F', '0909-000-0131', 'Filipino', 'Poblacion, Aurora, ZDS', 'Poblacion', '15/03/2020', ' CDO', 'N/A', 'PUM', NULL, NULL),
(133, 'ALONA', 'SIAO', 'N/A', '29', 'F', '0909-000-0132', 'Filipino', 'Poblacion, Aurora, ZDS', 'Poblacion', '15/03/2020', ' CDO', 'N/A', 'PUM', NULL, NULL),
(134, 'GLYZA MAE', 'VILLORIA', 'N/A', '20', 'F', '0909-000-0133', 'Filipino', 'P. DONA AURORA, POB.', 'Poblacion', '14/03/2020', ' MANILA', 'N/A', 'PUM', NULL, NULL),
(135, 'GLENNA WAYNE', 'VILLORIA', 'N/A', '19', 'F', '0909-000-0134', 'Filipino', 'P. DONA AURORA, POB.', 'Poblacion', '14/03/2020', ' MANILA', 'N/A', 'PUM', NULL, NULL),
(136, 'JUDEA WENCY', 'GOMONIT', 'N/A', '19', 'F', '0909-000-0135', 'Filipino', 'P. MAUSWAGON, POB.', 'Poblacion', '15/03/2020', ' CDO', 'N/A', 'PUM', NULL, NULL),
(137, 'RHUENINO CELESTINE', 'DICON', 'N/A', '30', 'M', '0909-000-0136', 'Filipino', 'Poblacion, Aurora, ZDS', 'Poblacion', '14/03/2020', 'MANILA', 'N/A', 'PUM', NULL, NULL),
(138, 'CHRISTIAN FRANCIS', 'BATOCTOY', 'N/A', '21', 'M', '0909-000-0137', 'Filipino', 'Poblacion, Aurora, ZDS', 'Poblacion', '15/03/2020', ' CEBU', 'N/A', 'PUM', NULL, NULL),
(139, 'SHIPREAD', 'PEREZ', 'N/A', '20', 'M', '0909-000-0138', 'Filipino', 'P. CALUBIU, POB.', 'Poblacion', '15/03/2020', 'CEBU', 'N/A', 'PUM', NULL, NULL),
(140, 'MARIE FRANCIS', 'TAN-ABRIA', 'N/A', 'N/A', 'F', '0909-000-0139', 'Filipino', 'Poblacion, Aurora, ZDS', 'Poblacion', '13/03/2020', ' MANILA', 'N/A', 'PUM', NULL, NULL),
(141, 'ATILANO', 'CORTES', 'N/A', '31', 'M', '0909-000-0140', 'Filipino', 'Poblacion, Aurora, ZDS', 'Poblacion', '13/03/2020', ' MANILA', 'N/A', 'PUM', NULL, NULL),
(142, 'BERNYREL PE?A', 'NUEVA', 'N/A', '31', 'F', '0909-000-0141', 'Filipino', 'P. MAUSWAGON, POB.', 'Poblacion', '13/03/2020', ' MANILA', 'N/A', 'PUM', NULL, NULL),
(143, 'HERBIE ROSE', 'CORTES', 'N/A', '24', 'F', '0909-000-0142', 'Filipino', 'Poblacion, Aurora, ZDS', 'Poblacion', '10/03/2020', ' PALAWAN', 'WITH COUGH BEFORE TRAVEL', 'PUM', NULL, NULL),
(144, 'DONA', 'CONEJOS', 'N/A', '28', 'F', '0909-000-0143', 'Filipino', 'Poblacion, Aurora, ZDS', 'Poblacion', '04/03/2020', ' USA', 'N/A', 'PUM', NULL, NULL),
(145, 'NATHANIEL', 'CONEJOS', 'N/A', '8', 'M', '0909-000-0144', 'Filipino', 'Poblacion, Aurora, ZDS', 'Poblacion', '04/03/2020', 'MANILA', 'N/A', 'PUM', NULL, NULL),
(146, 'THELMA', 'CONEJOS', 'N/A', '62', 'F', '0909-000-0145', 'Filipino', 'Poblacion, Aurora, ZDS', 'Poblacion', '04/03/2020', 'MANILA', 'N/A', 'PUM', NULL, NULL),
(147, 'GALILEO', 'TIGTIG', 'N/A', '34', 'M', '0909-000-0146', 'Filipino', 'Poblacion, Aurora, ZDS', 'Poblacion', '12/03/2020', ' MANILA', 'N/A', 'PUM', NULL, NULL),
(148, 'FILMA', 'QUI?O', 'N/A', '20', 'F', '0909-000-0147', 'Filipino', 'Poblacion, Aurora, ZDS', 'Poblacion', '15/03/2020', ' CEBU', 'N/A', 'PUM', NULL, NULL),
(149, 'Elsie', 'Angeles', 'N/A', '42', 'F', '0909-000-0148', 'Filipino', 'San Jose, Aurora, ZDS', 'Poblacion', '05/03/2020', ' Baguio City', 'N/A', 'PUM', NULL, NULL),
(150, 'Princess Elise', 'Angeles', 'N/A', '6', 'F', '0909-000-0149', 'Filipino', 'San Jose, Aurora, ZDS', 'Poblacion', '05/03/2020', ' Baguio City', 'N/A', 'PUM', NULL, NULL),
(151, 'REY', 'VELASQUEZ', 'N/A', '53', 'M', '0909-000-0150', 'FILIPINO', 'POBLACION, AZDS', 'Poblacion', '15/03/2020', ' CEBU', 'N/A', 'PUM', NULL, NULL),
(152, 'KERLVINN PAUL', 'VELASQUEZ', 'N/A', '30', 'M', '0909-000-0151', 'FILIPINO', 'POBLACION, AZDS', 'Poblacion', '15/03/2020', 'CEBU', 'N/A', 'PUM', NULL, NULL),
(153, 'VERNBODETH', ' CODENIERA', 'N/A', '24', 'F', '0909-000-0152', 'FILIPINO', 'POBLACION, AZDS', 'Poblacion', '15/03/2020', 'CEBU', 'N/A', 'PUM', NULL, NULL),
(154, 'MICHELLE', 'MALINGI', ' B.', '32', 'F', '0909-000-0153', 'FILIPINO', 'POBLACION, AZDS', 'Poblacion', '02/03/2020', ' THAILAND', 'COMPLETED', 'PUM', NULL, NULL),
(155, 'KYLE IAH', 'MALINGI', 'N/A', 'N/A', 'N/A', '0909-000-0154', 'FILIPINO', 'POBLACION, AZDS', 'Poblacion', '02/03/2020', ' THAILAND', 'COMPLETED', 'PUM', NULL, NULL),
(156, 'KAREN ROSE', 'IWAY', 'N/A', '19', 'F', '0909-000-0155', 'FILIPINO', 'P. IMELDA, POB.', 'Poblacion', '15/03/2020', ' CDO', 'N/A', 'PUM', NULL, NULL),
(157, 'NICOLE ANNE', 'OLEDAN', 'N/A', '19', 'F', '0909-000-0156', 'FILIPINO', 'P. STA. TERESITA, POB.', 'Poblacion', '15/03/2020', 'CDO', 'N/A', 'PUM', NULL, NULL),
(158, 'CLINT BRYNER', 'ZANORIA', 'N/A', '27', 'M', '0909-000-0157', 'FILIPINO', 'POBLACION, AZDS', 'Poblacion', '14/03/2020', ' MANILA', 'N/A', 'PUM', NULL, NULL),
(159, 'MARIELLE AIRA', 'SABADO', 'N/A', '23', 'F', '0909-000-0158', 'FILIPINO', 'POBLACION, AZDS', 'Poblacion', '15/03/2020', ' CEBU', 'N/A', 'PUM', NULL, NULL),
(160, 'FARREN AICEL', 'ABIL', 'N/A', '18', 'F', '0909-000-0159', 'FILIPINO', 'POBLACION, AZDS', 'Poblacion', '15/03/2020', ' CDO', 'N/A', 'PUM', NULL, NULL),
(161, 'VIRGINIA', 'BANAAG', 'N/A', '58', 'F', '0909-000-0160', 'FILIPINO', 'POBLACION, AZDS', 'Poblacion', '14/03/2020', ' ILO-ILO', 'N/A', 'PUM', NULL, NULL),
(162, 'NANETTE', 'REDELOSA', 'N/A', '59', 'F', '0909-000-0161', 'FILIPINO', 'P. DAHLIA, POB, AZDS', 'Poblacion', '14/03/2020', 'ILO-ILO', 'N/A', 'PUM', NULL, NULL),
(163, ' JUSTINE MAE', 'LAGUE', 'N/A', '22', 'F', '0909-000-0162', 'FILIPINO', 'P. DAHLIA, POB, AZDS', 'Poblacion', '14/03/2020', ' MANILA', 'N/A', 'PUM', NULL, NULL),
(164, 'ELAINE JOICE', 'DIGAL', 'N/A', '22', 'F', '0909-000-0163', 'FILIPINO', 'POBLACION, AZDS', 'Poblacion', '16/03/2020', ' CDO', 'N/A', 'PUM', NULL, NULL),
(165, 'EUNICE PEARL', 'DIGAL', 'N/A', '20', 'F', '0909-000-0164', 'FILIPINO', 'POBLACION, AZDS', 'Poblacion', '16/03/2020', ' CDO', 'N/A', 'PUM', NULL, NULL),
(166, ' NATHANIEL MARK CELESTINE', 'DICON', 'N/A', '20', 'M', '0909-000-0165', 'FILIPINO', 'POBLACION, AZDS', 'Poblacion', '16/03/2020', ' CDO', 'N/A', 'PUM', NULL, NULL),
(167, ' ELLA KRISTIN', 'CENIZA', 'N/A', '20', 'F', '0909-000-0166', 'FILIPINO', 'POBLACION, AZDS', 'Poblacion', '14/03/2020', ' CEBU', 'N/A', 'PUM', NULL, NULL),
(168, 'STEPHEN JOHN', 'SONGALIA', 'N/A', '19', 'M', '0909-000-0167', 'FILIPINO', 'POBLACION, AZDS', 'Poblacion', '16/03/2020', 'CEBU', 'N/A', 'PUM', NULL, NULL),
(169, 'JOHN CARVY', 'JAVIER', 'N/A', '20', 'M', '0909-000-0168', 'FILIPINO', 'POBLACION, AZDS', 'Poblacion', '15/03/2020', ' CEBU', 'N/A', 'PUM', NULL, NULL),
(170, 'ROSE IRA', 'JAVIER', 'N/A', '23', 'F', '0909-000-0169', 'FILIPINO', 'POBLACION, AZDS', 'Poblacion', '14/03/2020', ' CEBU', 'N/A', 'PUM', NULL, NULL),
(171, 'DEOSDIDIT', 'MABAYO', 'N/A', '50', 'M', '0909-000-0170', 'FILIPINO', 'POBLACION, AZDS', 'Poblacion', '16/03/2020', ' USA, CEBU, DAPITAN', 'N/A', 'PUM', NULL, NULL),
(172, 'ANTONIO', 'BRIDES', 'N/A', '58', 'M', '0909-000-0171', 'FILIPINO', 'POBLACION, AZDS', 'Poblacion', '15/03/2020', ' CDO', 'N/A', 'PUM', NULL, NULL),
(173, 'LUZ', 'BRIDES', 'N/A', '', '', '0909-000-0172', 'FILIPINO', 'POBLACION, AZDS', 'Poblacion', '15/03/2020', 'CDO', 'N/A', 'PUM', NULL, NULL),
(174, 'VERONICA', 'BRIDES', 'N/A', '28', 'F', '0909-000-0173', 'FILIPINO', 'POBLACION, AZDS', 'Poblacion', '15/03/2020', 'CDO', 'N/A', 'PUM', NULL, NULL),
(175, 'VICTORIA', 'MAGO', 'N/A', '74', 'F', '0909-000-0174', 'FILIPINO', 'POBLACION, AZDS', 'Poblacion', '13/03/2020', ' CDO', 'N/A', 'PUM', NULL, NULL),
(176, 'JUSTIN MAR', 'TAGALOG', 'N/A', '25', 'M', '0909-000-0175', 'FILIPINO', 'POBLACION, AZDS', 'Poblacion', '15/03/2020', ' MANILA', 'N/A', 'PUM', NULL, NULL),
(177, 'HESAH MAE', 'TAN', 'N/A', '18', 'F', '0909-000-0176', 'FILIPINO', 'POBLACION, AZDS', 'Poblacion', '14/03/2020', ' MANILA', 'N/A', 'PUM', NULL, NULL),
(178, 'LALAINE BEL', 'TAN', 'N/A', '37', 'F', '0909-000-0177', 'FILIPINO', 'POBLACION, AZDS', 'Poblacion', '13/03/2020', ' TUGUEGARAO', 'N/A', 'PUM', NULL, NULL),
(179, 'RAYA MARIE', 'CENIZA', ' F. ', '11', 'F', '0909-000-0178', 'FILIPINO', 'POBLACION, AZDS', 'Poblacion', '13/03/2020', ' TUGUEGARAO', 'N/A', 'PUM', NULL, NULL),
(180, 'MA. REGINA', 'FILIPINO', ' O. ', '48', 'F', '0909-000-0179', 'FILIPINO', 'POBLACION, AZDS', 'Poblacion', '13/03/2020', ' TUGUEGARAO', 'N/A', 'PUM', NULL, NULL),
(181, 'QUIMBERLY', 'LUMOSAD', 'N/A', '12', 'F', '0909-000-0180', 'FILIPINO', 'P. CALUBE, POB. AZDS', 'Poblacion', '13/03/2020', ' TUGUEGARAO', 'N/A', 'PUM', NULL, NULL),
(182, 'MARC TRISTAN', 'LLACER', 'N/A', '10', 'M', '0909-000-0181', 'FILIPINO', 'POBLACION, AZDS', 'Poblacion', '13/03/2020', ' TUGUEGARAO', 'N/A', 'PUM', NULL, NULL),
(183, 'GLAD MARCEL', 'CABAHUG', 'N/A', '10', 'F', '0909-000-0182', 'FILIPINO', 'POBLACION, AZDS', 'Poblacion', '13/03/2020', ' TUGUEGARAO', 'N/A', 'PUM', NULL, NULL),
(184, 'JAKE', 'SABELLANO', ' M. ', '43', 'M', '0909-000-0183', 'FILIPINO', 'POBLACION, AZDS', 'Poblacion', '13/03/2020', ' TUGUEGARAO', 'N/A', 'PUM', NULL, NULL),
(185, 'CECILE', 'SABELLANO', 'N/A', '42', 'F', '0909-000-0184', 'FILIPINO', 'POBLACION, AZDS', 'Poblacion', '13/03/2020', ' TUGUEGARAO', 'N/A', 'PUM', NULL, NULL),
(186, 'JACIL', 'SABELLANO', 'N/A', '12', 'F', '0909-000-0185', 'FILIPINO', 'POBLACION, AZDS', 'Poblacion', '13/03/2020', ' TUGUEGARAO', 'N/A', 'PUM', NULL, NULL),
(187, 'JOBIE', 'TALATAYOD', 'N/A', '38', 'F', '0909-000-0186', 'FILIPINO', 'POBLACION, AZDS', 'Poblacion', '12/03/2020', ' MANILA', 'N/A', 'PUM', NULL, NULL),
(188, 'KARLL ZEDHRICK', 'PENSERGA', 'N/A', '11', 'M', '0909-000-0187', 'FILIPINO', 'POBLACION, AZDS', 'Poblacion', '13/03/2020', ' TUGUEGARAO', 'N/A', 'PUM', NULL, NULL),
(189, 'FLORDELIZA', 'DENSING', 'N/A', 'N/A', 'N/A', '0909-000-0188', 'FILIPINO', 'POBLACION, AZDS', 'Poblacion', '13/03/2020', ' TUGUEGARAO', 'N/A', 'PUM', NULL, NULL),
(190, 'JAMES ROLAND', 'MAGLANGIT', 'N/A', '19', 'M', '0909-000-0189', 'FILIPINO', 'P. CALUBE, POB. AZDS', 'Poblacion', '16/03/2020', ' CEBU', 'N/A', 'PUM', NULL, NULL),
(191, 'FRANCIS', 'IROG-IROG', 'N/A', '20', 'M', '0909-000-0190', 'FILIPINO', 'P. MASAGANA, POB. AZDS', 'Poblacion', '16/03/2020', 'CEBU', 'N/A', 'PUM', NULL, NULL),
(192, 'MARY CLAIR', 'MI?OZA', 'N/A', '17', 'F', '0909-000-0191', 'FILIPINO', 'P. TUBURAN, POB. AZDS', 'Poblacion', '14/03/2020', ' DUMAGUETE', 'N/A', 'PUM', NULL, NULL),
(193, 'PEACHIE', 'UNDAG', 'N/A', '37', 'F', '0909-000-0192', 'FILIPINO', 'P. MADASIGON, POB. AZDS', 'Poblacion', '16/03/2020', ' CEBU', 'N/A', 'PUM', NULL, NULL),
(194, 'CATHERINE', 'ORLANES', 'N/A', '23', 'F', '0909-000-0193', 'FILIPINO', 'P. IMELDA, POB. AZDS', 'Poblacion', '12/03/2020', ' ILO-ILO', 'N/A', 'PUM', NULL, NULL),
(195, 'ANA MAE', 'RAMOS', 'N/A', '25', 'F', '0909-000-0194', 'FILIPINO', 'P. STA. TERESITA, POB. AZDS', 'Poblacion', '13/03/2020', ' CAVITE', 'N/A', 'PUM', NULL, NULL),
(196, 'NINIA DIEN', 'RAMOS', 'N/A', '2', 'F', '0909-000-0195', 'FILIPINO', 'P. STA. TERESITA, POB. AZDS', 'Poblacion', '13/03/2020', ' CAVITE', 'N/A', 'PUM', NULL, NULL),
(197, 'LANCE', 'CENIZA', 'N/A', '20', 'M', '0909-000-0196', 'FILIPINO', 'P. STA. TERESITA, POB. AZDS', 'Poblacion', '16/03/2020', 'CDO', 'N/A', 'PUM', NULL, NULL),
(198, 'CESAR IAN', 'ALASTRA', 'N/A', '26', 'M', '0909-000-0197', 'FILIPINO', 'POBLACION, AZDS', 'Poblacion', '14/03/2020', 'ILO-ILO', 'N/A', 'PUM', NULL, NULL),
(199, 'JEREMIE', 'PANGANDIAN', 'N/A', '20', 'M', '0909-000-0198', 'FILIPINO', 'POBLACION, AZDS', 'Poblacion', '16/03/2020', 'CDO', 'N/A', 'PUM', NULL, NULL),
(200, 'DARELL', 'DENOSTA', 'N/A', '31', 'M', '0909-000-0199', 'FILIPINO', 'P. STA. TERESITA, POB. AZDS', 'Poblacion', '15/03/2020', 'NCR', 'N/A', 'PUM', NULL, NULL),
(201, 'RHONNA CHESSA', 'DENOSTA', 'N/A', '30', 'F', '0909-000-0200', 'FILIPINO', 'P. STA. TERESITA, POB. AZDS', 'Poblacion', '15/03/2020', 'NCR', 'N/A', 'PUM', NULL, NULL),
(202, 'BRENT DUSTINE', 'DENOSTA', 'N/A', '9', 'M', '0909-000-0201', 'FILIPINO', 'P. STA. TERESITA, POB. AZDS', 'Poblacion', '15/03/2020', ' NCR', 'N/A', 'PUM', NULL, NULL),
(203, 'HARO DABREN', 'DENOSTA', 'N/A', '3', 'M', '0909-000-0202', 'FILIPINO', 'P. STA. TERESITA, POB. AZDS', 'Poblacion', '15/03/2020', 'NCR', 'N/A', 'PUM', NULL, NULL),
(204, 'RHOANN DENISE', 'DENOSTA', 'N/A', '1', 'F', '0909-000-0203', 'FILIPINO', 'P. STA. TERESITA, POB. AZDS', 'Poblacion', '15/03/2020', 'NCR', 'N/A', 'PUM', NULL, NULL),
(205, 'JIMMY', 'BONTIA', 'N/A', '38', 'F', '0909-000-0204', 'FILIPINO', 'P. CALUBE, POB. AZDS', 'Poblacion', '15/03/2020', 'CDO', 'N/A', 'PUM', NULL, NULL),
(206, 'SALOME', 'BONTIA', 'N/A', '38', 'F', '0909-000-0205', 'FILIPINO', 'P. CALUBE, POB. AZDS', 'Poblacion', '15/03/2020', ' CDO', 'N/A', 'PUM', NULL, NULL),
(207, 'IRENE', 'LIMBAGA', 'N/A', '35', 'F', '0909-000-0206', 'FILIPINO', 'P. BAGONG LIPUNAN, POB.', 'Poblacion', '15/03/2020', ' MANILA', 'N/A', 'PUM', NULL, NULL),
(208, ' STEPHANY', 'GUTTIEREZ', 'N/A', '27', 'F', '0909-000-0207', 'FILIPINO', 'P. IMELDA, POB. AZDS', 'Poblacion', '14/03/2020', ' MANILA', 'N/A', 'PUM', NULL, NULL),
(209, 'FRANK ANTHONY', 'ABUNDIENTE', 'N/A', '26', 'M', '0909-000-0208', 'FILIPINO', 'P. IMELDA, POB. AZDS', 'Poblacion', '14/03/2020', ' MANILA', 'N/A', 'PUM', NULL, NULL),
(210, 'LUCHIN', 'PUEBLOS', 'N/A', 'N/A', 'N/A', '0909-000-0209', 'FILIPINO', 'P. WALING, POB. AZDS', 'Poblacion', '15/03/2020', ' ILIGAN', 'N/A', 'PUM', NULL, NULL),
(211, 'SHAINE NICOLE', 'FLORES', 'N/A', '20', 'F', '0909-000-0210', 'FILIPINO', 'POBLACION, AZDS', 'Poblacion', '16/03/2020', ' CEBU', 'N/A', 'PUM', NULL, NULL),
(212, 'Gjeelard', 'Dacanay ', 'N/A', '7', 'F', '0909-000-0211', 'Filipino', 'San Jose, Aurora, ZDS', 'San Jose', '11/03/2020', ' Baguio City', 'COUGH & COLDS  before travelling', 'PUI', NULL, '2020-03-17 23:24:21'),
(213, 'SHANNEN DHANA', 'BULACLAC', 'N/A', '20', 'F', '0909-000-0212', 'Filipino', 'Poblacion, Aurora, ZDS', 'Poblacion', '14/03/2020', 'CEBU CITY', '4 days cough, sore throat, colds (3/10/2020)', 'PUI', NULL, NULL),
(214, 'LOUIS ANTON', 'BRIDES', 'N/A', '17', 'M', '0909-000-0213', 'FILIPINO', 'POBLACION, AZDS', 'Poblacion', '15/03/2020', 'CDO', 'COUGH', 'PUI', NULL, NULL),
(215, 'AURLAN', 'OSTREA', 'N/A', '31', 'M', '0909-000-0214', 'FILIPINO', 'P. MAUSWAGON, POB. AZDS', 'Poblacion', '12/03/2020', 'CDO', 'BODY MALAISE, HEADACHE, ITCHY THROAT', 'PUI', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `usertype` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `username`, `email`, `email_verified_at`, `password`, `usertype`, `status`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'CENIZA GEGEJOSPER B', 'admin', 'gegejosper@gmail.com', NULL, '$2y$10$tcxVkbSphiaRwTvmTxZp9eKLYFfQawxgszyHGVtg1HnQokdyVDdtG', 'admin', 'active', NULL, '2020-03-17 17:14:09', '2020-03-17 17:14:09'),
(6, 'GWAPO', 'gwapo', 'gwapo@gmail.com', NULL, '$2y$10$ZSo9od0aaoIJacgoWkInAODypHRvESyDtCSWfzoijVDU6JtlNyDRi', 'brgy', 'active', NULL, '2020-03-18 05:57:01', '2020-03-18 05:57:01');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `brgyusers`
--
ALTER TABLE `brgyusers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `patients`
--
ALTER TABLE `patients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `brgyusers`
--
ALTER TABLE `brgyusers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `patients`
--
ALTER TABLE `patients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=219;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
